class Hamburger {
    constructor(size, stuffing) {
        try {
            if (size && stuffing && arguments.length) {
                this._sizeBurger = size;
                this._stuffingBurger = stuffing;
                this._topping = [];
            } else {
                throw new HamburgerException('Mistake in arguments of Hamburger function');
            }

        } catch (error) {
            console.error(error._name + ": " + error._message);
        }
    }

    addTopping(topping) {
        try {
            if (!this._topping.includes(topping)) {
                this._topping.push(topping);
            } else {
                throw new HamburgerException('Duplicate topping')
            }
        } catch (error) {
            console.error(error._name + ": " + error._message);
        }
    };

    removeTopping(topping) {
        try {
            if (this._topping.includes(topping)) {
                this._topping.splice(this._topping.indexOf(topping), 1);
            } else {
                throw new HamburgerException('This topping dont added');
            }
        } catch (error) {
            console.error(error._name + ": " + error._message);
        }
    };

    get getToppings() {
        const array = [];
        for (let i = 0; i < this._topping.length; i++) {
            array.push(this._topping[i].name)
        }
        return array;
    };

    get getSize() {
        return this._sizeBurger.size;
    };

    get getStuffing() {
        return this._stuffingBurger.name;
    };

    calculatePrice() {
        let price = this._sizeBurger.price + this._stuffingBurger.price;
        let toppingArray = this._topping;
        for (let i = 0; i < toppingArray.length; i++) {
            price += toppingArray[i].price;
        }
        return price;
    };

    calculateCalories() {
        let calories = this._sizeBurger.calories + this._stuffingBurger.calories;
        let toppingArray = this._topping;
        for (let i = 0; i < toppingArray.length; i++) {
            calories += toppingArray[i].calories;
        }
        return calories;
    };

}

function HamburgerException(message) {
    this._message = message;
    this._name = 'HamburgerException';
}

Hamburger.SIZE_SMALL = {size: 'small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: 'large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'mayo', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: 'spice', price: 15, calories: 0};

let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Toppings:', hamburger.getToppings);
console.log('Size: %s', hamburger.getSize);
console.log('Stuffing: %s', hamburger.getStuffing);
console.log('Price: %d', hamburger.calculatePrice());
console.log('Calories: %d', hamburger.calculateCalories());
console.log("Have %d toppings", hamburger.getToppings.length);

