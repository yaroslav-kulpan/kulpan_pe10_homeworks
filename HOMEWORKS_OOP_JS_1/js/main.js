function Hamburger(size, stuffing) {
    try {
        if (size && stuffing && arguments.length) {
            this._sizeBurger = size;
            this._stuffingBurger = stuffing;
            this._topping = [];
        } else {
            throw new HamburgerException('Arguments of object failure');
        }

    } catch (error) {
        console.error(error._name + ": " + error._message);
    }
}

Hamburger.SIZE_SMALL = {size: 'small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: 'large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'mayo', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: 'spice', price: 15, calories: 0};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!this._topping.includes(topping)) {
            this._topping.push(topping);
        } else {
            throw new HamburgerException('topping has been added')
        }
    } catch (error) {
        console.error(error._name + ": " + error._message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this._topping.includes(topping)) {
            this._topping.splice(this._topping.indexOf(topping), 1);
        } else {
            throw new HamburgerException('Topping not added');
        }
    } catch (error) {
        console.error(error._name + ": " + error._message);
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    const array = [];
    for (let i = 0; i < this._topping.length; i++) {
        array.push(this._topping[i].name)
    }
    return array;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this._sizeBurger.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this._stuffingBurger.name;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let price = this._sizeBurger.price + this._stuffingBurger.price;
    let toppingArray = this._topping;
    for (let i = 0; i < toppingArray.length; i++) {
        price += toppingArray[i].price;
    }
    return price;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let calories = this._sizeBurger.calories + this._stuffingBurger.calories;
    let toppingArray = this._topping;
    for (let i = 0; i < toppingArray.length; i++) {
        calories += toppingArray[i].calories;
    }
    return calories;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this._message = message;
    this._name = 'HamburgerException';
}

let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log('Toppings:', hamburger.getToppings());
console.log('Size: %s', hamburger.getSize());
console.log('Stuffing: %s', hamburger.getStuffing());
console.log('Price: %d', hamburger.calculatePrice());
console.log('Calories: %d', hamburger.calculateCalories());
console.log("Have %d toppings", hamburger.getToppings().length);

