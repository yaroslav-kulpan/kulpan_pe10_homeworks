const request = new XMLHttpRequest();
request.open("GET", "https://swapi.co/api/films/");
request.send();
request.onload = function () {
    if (request.status !== 200) {
        console.log(request.statusText);
    } else {
        const resonsoseToJSON = JSON.parse(request.response);
        for (let i = 0; i < resonsoseToJSON.results.length; i++) {
            const container = document.getElementById('star-wars-content');
            const filmTitle = document.createElement('div');
            filmTitle.setAttribute('class', 'column__flex__4');
            filmTitle.innerHTML = `<h3 class="film__title">Film Title: ${resonsoseToJSON.results[i].title}</h3>
            <h4 class="film__episode">Episode: ${resonsoseToJSON.results[i].episode_id}</h4>
            <p class="film__short-description">Opening: ${resonsoseToJSON.results[i].opening_crawl}</p>`;
            container.appendChild(filmTitle);
            const showCharacters = document.createElement('a');
            showCharacters.className = 'show-btn';
            showCharacters.setAttribute('data-episode', `${resonsoseToJSON.results[i].episode_id}`);
            showCharacters.innerText = 'Show characters';
            showCharacters.addEventListener('click', showCharter);
            filmTitle.appendChild(showCharacters);
            const charactersContainer = document.createElement('div');
            charactersContainer.className = 'container-charters';
            filmTitle.appendChild(charactersContainer);
        }
    }
};

function showCharter() {
    this.style.display = 'none';
    const containerChar = document.getElementsByClassName('container-charters');
    const resonsoseToJSON = JSON.parse(request.response);
    for (let i = 0; i < resonsoseToJSON.results.length; i++) {
        for (let prop in resonsoseToJSON.results[i]) {
            if (resonsoseToJSON.results[i][prop] == this.dataset.episode) {
                for (let j = 0; j < resonsoseToJSON.results[i].characters.length; j++) {
                    let req = new XMLHttpRequest();
                    req.open('GET', `${resonsoseToJSON.results[i].characters[j]}`);
                    req.send();
                    req.onload = function () {
                        if (req.status !== 200) {
                            console.log(`Ошибка ${req.status}: ${req.statusText}`);
                        } else {
                            const arrName = JSON.parse(req.response);
                            const char = document.createElement("li");
                            char.textContent = arrName.name;
                            containerChar[i].appendChild(char);
                        }
                    };
                }
            }
        }
    }
}

